cmake_minimum_required(VERSION 2.8)

#---Common Geant4 CTest script----------------------------------------------
include(${CTEST_SCRIPT_DIRECTORY}/common.cmake)

set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_COMMAND "make -k -j all")

set(CTEST_BUILD_NAME "$ENV{BUILDNAME}")

set(options -DCMAKE_INSTALL_PREFIX=../install
            -DHEPMC_ENABLE_CPP11=ON
            -DHEPMC_ENABLE_ROOTIO=ON
            -DROOT_DIR=$ENV{ROOTDIR}
            -DHEPMC_BUILD_EXAMPLES=ON
            -DPYTHIA8_ROOT_DIR=$ENV{PYTHIA8_ROOT_DIR}
            -DCMAKE_VERBOSE_MAKEFILE=ON
)

# The build mode drives the name of the slot in cdash
ctest_start($ENV{MODE} TRACK $ENV{MODE})
ctest_configure(BUILD   "build"
                SOURCE  "source"
                OPTIONS "${options}")
ctest_build(BUILD ${CTEST_BINARY_DIRECTORY})
ctest_submit()




