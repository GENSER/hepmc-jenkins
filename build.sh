#!/bin/bash

# clean
rm -rf hepmc3 build install


ROOTDIR=/afs/cern.ch/sw/lcg/app/releases/ROOT/$ROOT_VERSION/$LCGPLAT
echo "label = $label"
if ! echo "$label" | grep -q 'mac'; then
  export PATH=/afs/cern.ch/sw/lcg/contrib/CMake/2.8.12.2/Linux-i386/bin:$PATH
  export CC=gcc
  export CXX=g++
  export FC=gfortran

  COMP_VERSION="$(echo $COMPILER | sed 's/gcc//g')"
  if test -s /afs/cern.ch/sw/lcg/contrib/gcc/$COMP_VERSION/x86_64-slc6/setup.sh ;then
    source /afs/cern.ch/sw/lcg/contrib/gcc/$COMP_VERSION/x86_64-slc6/setup.sh
    echo /afs/cern.ch/sw/lcg/contrib/gcc/$COMP_VERSION/x86_64-slc6/setup.sh sourced
  else
    echo "Native compiler will be used."
  fi
fi


url="http://git.cern.ch/pub/hepmc3"
git clone $url source

export MODE="HepMC3"
export PLATFORM=$LCGPLAT
export ROOTDIR=$ROOTDIR/$PLATFORM/root
export PYTHIA8_ROOT_DIR=$PYTHIA8_ROOT_DIR/$PLATFORM
# configure

export SITENAME="$(hostname)"
[ -z "$SITENAME" ] && export SITENAME="$HOSTNAME"
if [ -z "$PLATFORM" ] ;then
  export PLATFORM="$(python getPlatform.py)"
fi

export BUILDNAME="$(python getPlatform.py)"

bash jk-runbuild.sh

