#!/bin/bash

tester(){
  $@
  if [ $? -ne 0 ]; then
    echo "Execution of '$@' failed. "
    exit 1
  fi
}

echo " === EXAMPLES ==="

cd install/bin

tester ./basic_tree.exe

source pythia8_example_env.sh
./pythia8_example.exe pythia8_ee_to_Z_to_tautau.conf pythia8_output.hepmc3
wc -l pythia8_output.hepmc3

./HepMC3_fileIO_example.exe pythia8_output.hepmc3 fileIO_output
cat fileIO_output
