
#---Set the source and build directory--------------------------------------
set(CTEST_SOURCE_DIRECTORY "source")
set(CTEST_BINARY_DIRECTORY "build")

#---Set the install directory----------------------------------------------- 
# if $INSTALLDIR is given, use that one, otherwise derive from binary dir
set(CTEST_INSTALL_DIRECTORY "install")

#---Set the CTEST SITE according to the environment-------------------------
execute_process(COMMAND hostname OUTPUT_VARIABLE h OUTPUT_STRIP_TRAILING_WHITESPACE)
string(TOLOWER ${h} host)
set(CTEST_SITE "${host}")
set(CLIENT_SITE "${host}")

#---CDash settings----------------------------------------------------------
set(CTEST_PROJECT_NAME "HepMC3")
set(CTEST_NIGHTLY_START_TIME "01:00:00 UTC")
set(CTEST_DROP_METHOD "http")
set(CTEST_DROP_SITE "cdash.cern.ch")
set(CTEST_DROP_LOCATION "/submit.php?project=HepMC3")
set(CTEST_DROP_SITE_CDASH TRUE)

#---Custom CTest settings---------------------------------------------------
set(CTEST_CUSTOM_MAXIMUM_FAILED_TEST_OUTPUT_SIZE "1000000")
set(CTEST_CUSTOM_MAXIMUM_PASSED_TEST_OUTPUT_SIZE "100000")
set(CTEST_CUSTOM_MAXIMUM_NUMBER_OF_ERRORS "256")
set(CTEST_TEST_TIMEOUT 1500)


